#!/bin/bash

current_time=$(date "+%d-%m-%Y__%H-%M-%S")

#taking backup of existing files
rsync -aAXuv $HOME/.vimrc $HOME/.vimrc.backup.$current_time;
rsync -aAXuv $HOME/.vim $HOME/.vim.backup.$current_time;

#replacing the existing files with improved files
rsync -aAXuv .vimrc $HOME/;
rsync -aAXuv .vim $HOME/;
